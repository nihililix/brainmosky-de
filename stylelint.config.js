/**
 * Stylelint EXAMPLE config
 */
module.exports = {
    'plugins': [
        'stylelint-scss',
        'stylelint-order'
    ],
    'extends': [
        './config/stylelint.default.js',
        './config/stylelint.scss.js',
        './config/stylelint.order.js'
    ],
    'ignoreFiles': [
        // enter files to ignore (e.g. libs, styleguide styles)
        '**/node_modules/**'
    ],
    'defaultSeverity': 'warning',
    'rules': {
      'color-hex-case': 'upper',
      'indentation': 4,
      "declaration-block-single-line-max-declarations": 1,
      'selector-nested-pattern': '(?!@media)|&(?! &)|(&:)|(&::)|(--)|(@nest)'
    }
}
