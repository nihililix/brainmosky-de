### brainmosky-de
brainmosky-de

### Install

** Installation **
```
git clone https://gitlab.com/nihililix/brainmosky-de.git
```

### Frontend

** Installation **
```
yarn
```

** Build Prod Version **
```
yarn build
```

** Build Prod Version **

Copy `dist/index.html` and `dist/bundle.css` to your webspace

#### Features / Dependencies

* scss
* es6
* webpack
* doiuse
